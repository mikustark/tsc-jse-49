package ru.tsc.karbainova.tm.command.project;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;

public class ProjectUpdateByIndexCommand extends AbstractCommand {
    @Override
    public String name() {
        return "update-by-index-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update by index project";
    }

    @Override
    public void execute() {
        final String index = TerminalUtil.nextLine();
        final String name = TerminalUtil.nextLine();
        final String description = TerminalUtil.nextLine();
        SessionDTO session = serviceLocator.getSession();
//        serviceLocator.getProjectEndpoint().updateByIndexProject(session, Integer.parseInt(index), name, description);
    }
}
