package ru.tsc.karbainova.tm.api.service.dto;

import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.dto.ProjectDTO;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @SneakyThrows
    void clear();

    void addAll(Collection<ProjectDTO> projects);

    @SneakyThrows
    ProjectDTO add(ProjectDTO project);

    void create(String userId, String name);

    void create(String userId, String name, String description);

    void remove(String userId, ProjectDTO project);

    List<ProjectDTO> findAll();

//    List<Project> findAll(String userId);

//    List<Project> findAll(String userId, Comparator<Project> comparator);

    ProjectDTO updateById(String userId, String id, String name, String description);

//    Project updateByIndex(String userId, Integer index, String name, String description);

//    void removeById(String userId, String id);
//
//    void removeByIndex(String userId, Integer index);
//
//    void removeByName(String userId, String name);

//    Project findById(String userId, String id);

//    Project findByIndex(String userId, Integer index);

    ProjectDTO findByName(String userId, String name);

//    Project startById(String userId, String id);
//
//    Project startByIndex(String userId, Integer index);
//
//    Project startByName(String userId, String name);
//
//    Project finishById(String userId, String id);
//
//    Project finishByIndex(String userId, Integer index);
//
//    Project finishByName(String userId, String name);
}
