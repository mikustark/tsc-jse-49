package ru.tsc.karbainova.tm.dto;

import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.entity.IWBS;
import ru.tsc.karbainova.tm.enumerated.Status;
import ru.tsc.karbainova.tm.listener.JpaEntityListener;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "tm_task")
@EntityListeners(JpaEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TaskDTO extends AbstractOwnerDTOEntity implements IWBS {

    public TaskDTO() {
    }

    public TaskDTO(String name) {
        this.name = name;
    }

    public TaskDTO(String name, String description) {
        this.name = name;
        this.description = description;
    }


    @Column
    @NonNull
    private String name;
    @Column
    @Nullable
    private String description;
    @NonNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;
    @Nullable
    @Column(name = "project_id")
    private String projectId = null;
    @Nullable
    @Column(name = "start_date")
    private Date startDate;
    @Nullable
    @Column(name = "finish_date")
    private Date finishDate;
    @NonNull
    @Column
    private Date created = new Date();

    @Override
    public String toString() {
        return " " + name + " ";
    }

}
