package ru.tsc.karbainova.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.tsc.karbainova.tm.api.service.dto.IProjectService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.api.service.dto.IProjectToTaskService;
import ru.tsc.karbainova.tm.dto.ProjectDTO;
import ru.tsc.karbainova.tm.dto.SessionDTO;
import ru.tsc.karbainova.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class ProjectEndpoint extends AbstractEndpoint {

    public ProjectEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public void addProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "project") @NonNull ProjectDTO project) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().add(project);
    }

    @WebMethod
    public List<ProjectDTO> findAllProject(
            @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll();
    }

    @WebMethod
    public void createProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") @NonNull String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().create(session.getUserId(), name);
    }

    @WebMethod
    public void createProjectAllParam(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") @NonNull String name,
            @WebParam(name = "description") @NonNull String description) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().create(session.getUserId(), name, description);
    }

    @WebMethod
    public void removeProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "project") @NonNull ProjectDTO project) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().remove(session.getUserId(), project);
    }

//    @WebMethod
//    public List<Project> findAllProjectByUs(
//            @WebParam(name = "session") final Session session
//    ) {
//        serviceLocator.getSessionService().validate(session);
//        return serviceLocator.getProjectService().findAll(session.getUserId());
//    }

    @WebMethod
    public ProjectDTO findByNameProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") @NonNull String name) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByName(session.getUserId(), name);
    }

    @WebMethod
    public List<TaskDTO> findTaskByProjectIdProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") @NonNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectToTaskService().findTasksByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    public void taskBindByIdProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") @NonNull String projectId,
            @WebParam(name = "taskId") @NonNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectToTaskService().bindTaskByProjectId(session.getUserId(), projectId, taskId);
    }

    @WebMethod
    public void taskUnbindByIdProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") @NonNull String projectId,
            @WebParam(name = "taskId") @NonNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectToTaskService().unbindTaskByProjectId(session.getUserId(), projectId, taskId);
    }

    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") @NonNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectToTaskService().removeProjectById(projectId);
    }
}
